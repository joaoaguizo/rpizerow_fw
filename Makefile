default:
	rm -rf build
	mkdir build

	arm-none-eabi-gcc -mcpu=arm1176jzf-s -fpic -ffreestanding -c src/boot.S -o build/boot.o
	arm-none-eabi-gcc -mcpu=arm1176jzf-s -fpic -ffreestanding -std=gnu99 -c src/kernel.c -o build/kernel.o -O2 -Wall -Wextra

	arm-none-eabi-gcc -T src/linker.ld -o myos.elf -ffreestanding -O2 -nostdlib build/boot.o build/kernel.o -lgcc
	arm-none-eabi-objcopy myos.elf -O binary kernel7.img